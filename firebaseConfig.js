import firebase from "firebase/app";
import "firebase/messaging";

// Initialize Firebase
var config = {
  apiKey: "AIzaSyCkXHF9KI76x-H2T8KWgBB9ko3_Y8NcQuo",
  authDomain: "personal-task-management-db7dc.firebaseapp.com",
  databaseURL:
    "https://personal-task-management-db7dc-default-rtdb.firebaseio.com",
  projectId: "personal-task-management-db7dc",
  storageBucket: "personal-task-management-db7dc.appspot.com",
  messagingSenderId: "869499037151",
  appId: "1:869499037151:web:51e5743a4ef18ed420906a",
  measurementId: "G-2JK0GP6JWP"
};

firebase.initializeApp(config);
const messaging = firebase.messaging();

export const getToken = () => {
  return messaging
    .getToken({
      vapidKey:
        "BCRwfh6snBeoVwjtqeTVJLjCYe0_bUj5IArjRmSZ0dSnWYBZ09uLyCEIxJhUGfRieq8EVTUFtT_nqGT_njyoBXM"
    })
    .then(currentToken => {
      if (currentToken) {
        console.log("current token for clientt: ", currentToken);
        // Track the token -> client mapping, by sending to backend server
        // show on the UI that permission is secured
      } else {
        console.log(
          "No registration token available. Request permission to generate one."
        );
        // shows on the UI that permission is required
      }
    })
    .catch(err => {
      console.log("An error occurred while retrieving token. ", err);
      // catch error while creating client token
    });
};

export const onMessageListener = () =>
  new Promise(resolve => {
    messaging.onMessage(payload => {
      resolve(payload);
    });
  });
