export default {
  1: "#3FA9F5",
  2: "#3FA9F5",
  3: "#8CC63F",
  4: "#EF7B7B",
  5: "#CE9800",
  6: "#0071BC",
  7: "#006837",
  8: "#3FA9F5",
  9: "#993A61",
  10: "#B76F20",
  11: "#687A82",
  12: "#7E40B5",
  13: "#1B1464",
  14: "#969132",
  15: "#0071BC",
  16: "#0071BC",
  17: "#A67C52",
  18: "#F15A24",
  19: "#009245",
  20: "#827F80",
  21: "#F9BD6E",
  22: "#7D71F4",
  23: "#05527C",
  24: "#9B5C78",
  25: "#5056A3",
  26: "#739392",
  27: "#614793",
  28: "#DD6333",
  29: "#124860",
  30: "#97D169",
  31: "#61ADA2",
  32: "#7A7A7A",
  33: "#966977",
  34: "#487525",
  35: "#0071BC",
  36: "#D15A66",
  37: "#795996",
  38: "#351F1F",
  39: "#593B7F",
  40: "#7C4253",
  41: "#f9757f",
  42: "#EA6F49",
  43: "#E07514",
  44: "#39B54A",
  45: "#E83D3D",
  46: "#53C5D0",
  47: "#53840D",
  48: "#5252EF",
  49: "#9E846D",
  50: "#009245",
  51: "#CE6655",
  52: "#7596CC",
  53: "#823737",
  54: "#59879B",
  55: "#5B9975",
  56: "#2C631A",
  57: "#629AF4",
  60: "#45377A",
  61: "#2B136D",
  62: "#5768FF",
  63: "#7C4253",
  64: "#00989D",
  65: "#00989D",
  66: "#6A90CC",
  67: "#385D7F",
  68: "#934C69",
  69: "#45377A",
  70: "#61ADA2",
  71: "#1957C2",
  72: "#105066",
  73: "#7C7F7D",
  74: "#E83D3D",
  75: "#6BA6C9",
  76: "#72BF44",
  77: "#5E4E72",
  78: "#FF9161"
};
