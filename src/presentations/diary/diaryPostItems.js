export default [
  {
    title: "Học Tiếng Hàn 💓",
    desc: "Tuần học 50 từ mới",
    time: "Fri June 25 2021 07:46:05 GMT+0000 (GMT)",
    mood: 2,
    isLiked: true,
    text:
      "Tuần này tôi đã học được 70 từ, vượt ngoài mong đợi. Tôi hy vọng mình sẽ đạt 100 điểm trong bài kiểm tra trên lớp sắp tới. Tuần sau tôi sẽ nỗ lực hơn để đạt được mục tiêu của mình.",
    media: [
      {
        img: "https://miro.medium.com/max/1440/1*qgPlY2yHgC24xBaTjyC4CQ.jpeg"
      }
    ]
  },
  {
    title: "Tập thể dục",
    desc: "Rất vui luôn nhé",
    time: "Wed June 23 2021 12:05:05 GMT+0000 (GMT)",
    mood: 3,
    isLiked: true,
    text:
      "Tôi đang tích cực đến phòng tập và ăn theo chế độ. Tôi hy vọng tôi sẽ có được body mơ ước. Cố lên nào Joe oi",
    media: [
      {
        sources: [
          { type: "video/mp4", src: "http://vjs.zencdn.net/v/oceans.mp4" }
        ],
        poster: "https://goo.gl/xcCsDd"
      }
    ]
  },
  {
    title: "Đi dã ngoại",
    desc: "Amazing",
    time: "Tue June 01 2021 07:46:05 GMT+0000 (GMT)",
    mood: 4,
    isLiked: true,
    text:
      "Chuyến đi vui nhất từ trước đến nay. Tôi có thời gian chơi cùng với bọn trẻ. Tiệc nướng ngoài trời thật là tuyệt vời.",
    media: [
      {
        img:
          "https://natyandnolan.com/wp-content/uploads/2017/11/bali-beach-picnic.jpg"
      }
    ]
  }
];
