export default {
  // CompletionDistributionChart
  completionDistributionBarChart: {
    chartOptions: {
      grid: {
        show: false,
        padding: {
          left: 0,
          right: 0
        }
      },
      chart: {
        type: "bar",
        sparkline: { enabled: true },
        toolbar: { show: false }
      },
      // colors: [
      //   "#7367f0"
      //   // "rgba(115,103,240,0.15)",
      // ],
      states: {
        hover: {
          filter: "none"
        }
      },
      plotOptions: {
        bar: {
          columnWidth: "45%",
          distributed: true,
          endingShape: "rounded" // Deprecated
          // borderRadius: "20px" // Coming Soon
        }
      }
      // tooltip: {
      //   x: { show: false }
      // },
    }
  },
  // CompletionRateDistributionChart
  completionRateDistributionDonut: {
    chartOptions: {
      labels: ["Đúng giờ", "Trễ giờ", "Không đặt thời hạn", "Chưa hoàn thành"],
      dataLabels: {
        enabled: true,
        style: {
          fontSize: "14px",
          fontFamily: "Montserrat,Helvetica,Arial,serif",
          colors: ["#fff"]
        },
        dropShadow: {
          enabled: false
        }
      },
      legend: {
        show: false
        // position: "bottom"
      },
      chart: {
        offsetY: 30,
        type: "donut",
        toolbar: {
          show: false
        }
      },
      stroke: { width: 0 },
      colors: ["#7961F9", "#FF9F43", "#EA5455", "#00d4bd"],
      fill: {
        type: "gradient",
        gradient: {
          gradientToColors: ["#9c8cfc", "#FFC085", "#f29292", "#00d4bd"]
        }
      },
      plotOptions: {
        pie: {
          donut: {
            size: "65%",
            labels: {
              show: true,
              total: {
                show: true,
                showAlways: true,
                label: "Đã hoàn thành",
                fontFamily: "Montserrat,Helvetica,Arial,serif",
                fontSize: 18,
                color: "#00d4bd",
                formatter: function(w) {
                  return (
                    (
                      w.globals.seriesTotals.reduce((a, b) => {
                        return a + b;
                      }, 0) - w.globals.seriesTotals[3]
                    ).toFixed(1) + "%"
                  );
                }
              }
            }
          }
        }
      }
    }
  },
  // ClassifiedCompletionStatistics
  classifiedCompletionStatisticsDonut: {
    chartOptions: {
      dataLabels: {
        enabled: false,
        style: {
          fontSize: "14px",
          fontFamily: "Montserrat,Helvetica,Arial,serif",
          colors: ["#fff"]
        },
        dropShadow: {
          enabled: false
        }
      },
      legend: {
        show: false
        // position: "bottom"
      },
      chart: {
        offsetY: 30,
        type: "donut",
        toolbar: {
          show: false
        }
      },
      stroke: { width: 0 },
      plotOptions: {
        pie: {
          donut: {
            size: "85%",
            labels: {
              show: true,
              total: {
                show: true,
                showAlways: true,
                label: "Đã hoàn thành",
                fontFamily: "Montserrat,Helvetica,Arial,serif",
                fontSize: 14,
                color: "grey"
              }
            }
          }
        }
      }
    }
  }
};
