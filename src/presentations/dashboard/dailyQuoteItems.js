export default [
  {
    content:
      "Thinking is the hardest work there is, which is probably the reason so few engage in it.",
    author: "Henry Ford, American industrialist and founder of Ford Motor Co."
  },
  {
    content:
      "A dream does not become reality through magic; it takes sweat, determination, and hard work.",
    author: "Colin Powell, former U.S. Defense Secretary"
  },
  {
    content:
      "I’m a great believer in luck, and I find the harder I work, the more I have of it.",
    author: "Thomas Jefferson, former President of the United States"
  },
  {
    content: "No matter how hard you work, someone else is working harder.",
    author: "Elon Musk, entrepreneur"
  },
  {
    content: "Hard work beats talent if talent doesn’t work hard.",
    author: "Tim Notke, basketball coach"
  },
  {
    content: "Satisfaction lies in the effort, not in the attainment.",
    author: "Mahatma Gandhi, Indian activist"
  },
  {
    content:
      "We think, mistakenly, that success is the result of the amount of time we put in at work, instead of the quality of time we put in.",
    author: "Ariana Huffington, businesswoman and author"
  },
  {
    content:
      "Men die of boredom, psychological conflict and disease. They do not die of hard work.",
    author: "David Ogilvy, advertising business tycoon"
  },
  {
    content: "The only place where success comes before work is in the .",
    author: "Vidal Sassoon, hairdressing business tycoon"
  },
  {
    content:
      "Inaction breeds doubt and fear. Action breeds confidence and courage. If you want to conquer fear, do not sit home and think about it. Go out and get busy.",
    author: "Dale Carnegie, American business "
  },
  {
    content: "Work hard, have fun, make history.",
    author: "Jeff Bezos, Amazon "
  },
  {
    content: "I never took a day off in my 20s. Not one.",
    author: "Bill Gates, Microsoft co-founder"
  },
  {
    content:
      "There are no secrets to success. It is the result of preparation, hard work, and learning from failure.",
    author: "Colin Powell, former U.S. Defense Secretary"
  },
  {
    content: "Without labor, nothing prospers.",
    author: "Sophocles, philosopher"
  },
  {
    content:
      "There is no time for cut - and - dried monotony.There is time for work.And time for love.That leaves no other time.",
    author: "Coco Chanel, fashion icon"
  },
  {
    content:
      "The price of success is hard work, dedication to the job at hand, and the determination that whether we win or lose, we have applied the best of ourselves to the task at hand.",
    author: "Vince Lombardi, Hall of Fame coach"
  },
  {
    content:
      "Teamwork is the ability to work together toward a common vision. The ability to direct individual accomplishments toward organizational objectives. It is the fuel that allows common people to attain uncommon results.",
    author: "Andrew Carnegie, business magnate and philanthropist"
  },
  {
    content:
      "Great teamwork is the only way we create the breakthroughs that define our careers.",
    author: "Pat Riley, basketball coach"
  },
  {
    content:
      "The best teamwork comes from men who are working independently toward one goal in unison.",
    author: "J.C. Penney, retail magnate"
  },
  {
    content:
      "In the end, all business operation can be reduced to three words: people, products, and profits.Unless you’ve got a good team, you can’t do much with the other two.",
    author: "Lee Iacocca, President, CEO of Chrysler"
  },
  {
    content:
      "The way a team plays as a whole determines its success. You may have the greatest bunch of individual stars in the world, but if they don’t play together, the club won’t be worth a dime.",
    author: "Babe Ruth, baseball legend"
  },
  {
    content: "Success is best when it’s shared.",
    author: "Howard Schultz, CEO of Starbucks"
  },
  {
    content:
      "Great companies are built in the office, with hard work put in by a team.",
    author: "Emily Chang, journalist and executive producer"
  },
  {
    content:
      "If you love your work, you’ll be out there every day trying to do it the best you possibly can, and pretty soon everybody around will catch the passion from you – like a fever.",
    author: "Sam Walton, founder of Walmart"
  },
  {
    content: "Nothing is particularly hard if you divide it into small jobs.",
    author: "Henry Ford, American"
  },
  {
    content:
      "I know the price of success: dedication, hard work and an unremitting devotion to the things you want to see happen.",
    author: "Frank Lloyd Wright, architect and writer"
  },
  {
    content:
      "Doing the best at this moment puts you in the best place for the next moment.",
    author: "Oprah Winfrey"
  }
];
