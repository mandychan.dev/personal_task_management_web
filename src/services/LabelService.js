import BaseService from "../core/common/BaseService";

export default {
  getList() {
    return BaseService.get("/labels", {});
  },
  getDetail(labelId) {
    return BaseService.get(`/labels/${labelId}`, {});
  },
  create(data) {
    return BaseService.post("/labels", data, {});
  },
  update(labelId, data) {
    return BaseService.put(`/labels/${labelId}`, data, {});
  },
  delete(labelId) {
    return BaseService.delete(`/labels/${labelId}`, {});
  }
};
