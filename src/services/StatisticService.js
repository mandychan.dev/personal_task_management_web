import BaseService from "../core/common/BaseService";

export default {
  getReport() {
    return BaseService.get("/statistic/tasks/all", {});
  }
};
