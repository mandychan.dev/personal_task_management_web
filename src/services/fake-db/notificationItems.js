import { randomDate } from "../../core/utils/date";

export default [
  {
    index: 0,
    title: "Chào mừng Joe! 😃",
    msg: "Thật tuyệt khi có bạn online. Thưởng thức Totodo :)",
    icon: "MessageSquareIcon",
    time: randomDate({ sec: 10 }),
    category: "primary"
  },
  {
    index: 1,
    title: "Hi Joe, cùng xem lại 1 tháng của bạn 🎈",
    msg:
      "Bạn đã chạy bộ tổng cộng 10km trong vòng 1 tháng. Nỗ lực nhiều hơn để đạt phần thưởng nhé!",
    icon: "MessageSquareIcon",
    time: randomDate({ sec: 40 }),
    category: "primary",
    image:
      "https://images-na.ssl-images-amazon.com/images/I/41Olahra45L._AC_.jpg"
  },
  {
    index: 2,
    title: "Nhận phần thưởng 🎊 🎁 🎉",
    msg: "Bạn đã hoàn thành mục tiêu đạt 850 điểm TOIEC",
    icon: "PackageIcon",
    time: randomDate({ min: 1 }),
    category: "success",
    image:
      "https://static1.srcdn.com/wordpress/wp-content/uploads/2020/11/Apple-iPhone-12-Pro-Max-Holiday-BG.jpg"
  },
  {
    index: 3,
    title: "Nộp báo cáo cho sếp 🎯",
    msg: "Bạn cần hoàn thành ngay hôm nay!",
    icon: "MailIcon",
    time: randomDate({ min: 6 }),
    category: "primary"
  },
  {
    index: 4,
    title: "Meeting với team 🌂",
    msg: "Bạn có công việc cần thực hiện vào ngày mai",
    icon: "CalendarIcon",
    time: randomDate({ hr: 2 }),
    category: "warning"
  },
  {
    index: 5,
    title: "Đi siêu thị 🛒",
    msg: "Bạn đã bỏ lỡ 1 công việc hết hạn vào ngày hôm qua",
    icon: "AlertOctagonIcon",
    time: randomDate({ hr: 2 }),
    category: "danger"
  }
];
