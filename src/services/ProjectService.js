import BaseService from "../core/common/BaseService";

export default {
  getList() {
    return BaseService.get("/projects", {});
  },
  getDetail(projectId) {
    return BaseService.get(`/projects/${projectId}`, {});
  },
  create(data) {
    return BaseService.post("/projects", data, {});
  },
  update(projectId, data) {
    return BaseService.put(`/projects/${projectId}`, data, {});
  },
  delete(projectId) {
    return BaseService.delete(`/projects/${projectId}`, {});
  }
};
