import BaseService from "../core/common/BaseService";

export default {
  getList() {
    return BaseService.get("/tasks", {});
  },
  getDetail(taskId) {
    return BaseService.get(`/tasks/${taskId}`, {});
  },
  create(data) {
    const _data = { ...data, files: data.attachmentInfos };
    return BaseService.postFile("/tasks", _data, {});
  },
  update(taskId, data) {
    return BaseService.put(`/tasks/${taskId}`, data, {});
  }
};
