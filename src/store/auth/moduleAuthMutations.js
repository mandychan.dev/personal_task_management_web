/*=========================================================================================
  File Name: moduleAuthMutations.js
  Description: Auth Module Mutations
==========================================================================================*/

import axios from "../../services/http/axios/index.js";

export default {
  SET_BEARER(state, accessToken) {
    axios.defaults.headers.common["Authorization"] = "Bearer " + accessToken;
  },

  SET_SAVED_ACCOUNT(state, account) {
    localStorage.setItem("savedAccount", account);
  }
};
