/*=========================================================================================
  File Name: moduleTodoState.js
  Description: Todo Module State
==========================================================================================*/

export default {
  tasks: [],
  taskTags: [],
  taskProjects: [],
  todoSearchQuery: "",
  todoFilter: null,
  tagSearchQuery: "",
  statisticData: null
};
