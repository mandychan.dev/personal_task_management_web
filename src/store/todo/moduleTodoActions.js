/*=========================================================================================
  File Name: moduleTodoActions.js
  Description: Todo Module Actions
==========================================================================================*/

import axios from "@/axios.js";
import TaskService from "../../services/TaskService";
import ProjectService from "../../services/ProjectService";
import LabelService from "../../services/LabelService";
import StatisticService from "../../services/StatisticService";

export default {
  setTodoSearchQuery({ commit }, query) {
    commit("SET_TODO_SEARCH_QUERY", query);
  },

  async fetchTasks({ commit }, payload) {
    try {
      const { result } = await TaskService.getList({ filter: payload.filter }); // all | today | time period
      commit("SET_TASKS", result);
      return result;
    } catch (error) {
      console.log(error.message);
    }
    return [];
  },

  async fetchTags({ commit }, payload) {
    try {
      const { result } = await LabelService.getList(); // all | today | time period
      commit("SET_TAGS", result);
      return result;
    } catch (error) {
      console.log(error.message);
    }
    return [];
  },

  async fetchProjects({ commit }, payload) {
    try {
      const { result } = await ProjectService.getList(); // all | today | time period
      commit("SET_PROJECTS", result);
      return result;
    } catch (error) {
      console.log(error.message);
    }
    return [];
  },

  async fetchStatisticData({ commit }, payload) {
    try {
      const { result } = await StatisticService.getReport(); // all | today | time period
      commit("SET_STATISTIC_DATA", result);
      return result;
    } catch (error) {
      throw error;
    }
  },

  async addTask({ commit }, payload) {
    const { task, notify, callback } = payload;
    try {
      const { result, message } = await TaskService.create(task);

      // tam thoi => nen sua du lieu tra ve o backend
      const temp = await TaskService.getDetail(result._id);

      commit("ADD_TASK", Object.assign(temp.result, { id: result._id }));

      notify({
        title: "Thành công",
        text: message,
        iconPack: "feather",
        icon: "icon-check",
        color: "success"
      });
      if (callback) {
        callback();
      }
    } catch (error) {
      notify({
        title: "Thất bại",
        text: error.message,
        iconPack: "feather",
        icon: "icon-alert-circle",
        color: "danger"
      });
      throw error;
    }
  },

  async updateTask({ commit }, payload) {
    const { task, notify, callback } = payload;
    try {
      const { result, message } = await TaskService.update(task._id, task);

      // tam thoi => nen sua du lieu tra ve o backend
      const temp = await TaskService.getDetail(result._id);

      commit("UPDATE_TASK", temp.result);

      notify({
        title: "Thành công",
        text: message,
        iconPack: "feather",
        icon: "icon-check",
        color: "success"
      });
      if (callback) {
        callback();
      }
    } catch (error) {
      notify({
        title: "Thất bại",
        text: error.message,
        iconPack: "feather",
        icon: "icon-alert-circle",
        color: "danger"
      });
    }
  },

  setTagSearchQuery({ commit }, query) {
    commit("SET_TAG_SEARCH_QUERY", query);
  }
};
