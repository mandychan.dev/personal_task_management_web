/*=========================================================================================
  File Name: moduleTodoGetters.js
  Description: Todo Module Getters
==========================================================================================*/
import { now, isSameDay, isBeforeDay, isAfterDay } from "../../core/utils/date";
import { PriorityEnums, DayEnums } from "../../constants/enums";
import { groupBy } from "../../core/utils/list";
import moment from "moment";
import { convertSelectList } from "../../core/utils/list";

export default {
  queriedTasks: state =>
    state.tasks.filter(task => {
      let isItemOfCurrentFilter = false;

      if (
        (state.todoFilter === "all" && !task.isTrashed) ||
        (state.todoFilter === "important" &&
          !task.isTrashed &&
          task.isImportant) ||
        (state.todoFilter === "starred" && !task.isTrashed && task.isStarred) ||
        (state.todoFilter === "completed" &&
          !task.isTrashed &&
          task.isCompleted) ||
        (state.todoFilter === "trashed" && task.isTrashed) ||
        (task.labelIds.includes(state.todoFilter) && !task.isTrashed) ||
        (task.projectId == state.todoFilter && !task.isTrashed) ||
        (state.todoFilter === "high-priority" &&
          !task.isTrashed &&
          task.priority == PriorityEnums.HighPriority.id) ||
        (state.todoFilter === "medium-priority" &&
          !task.isTrashed &&
          task.priority == PriorityEnums.MediumPriority.id) ||
        (state.todoFilter === "low-priority" &&
          !task.isTrashed &&
          task.priority == PriorityEnums.LowPriority.id) ||
        (state.todoFilter === "no-priority" &&
          !task.isTrashed &&
          task.priority == PriorityEnums.NoPriority.id) ||
        (state.todoFilter === "today" &&
          /* isBeforeDay(Date(now()), task.dueDate) */ !task.isCompleted &&
          isBeforeDay(
            task.startDate && task.startDate != ""
              ? task.startDate
              : task.createdAt,
            Date(now())
          ) &&
          !task.isTrashed)
      ) {
        isItemOfCurrentFilter = true;
      }

      return (
        isItemOfCurrentFilter &&
        (task.name
          .toLowerCase()
          .includes(state.todoSearchQuery.toLowerCase()) ||
          task.description
            .toLowerCase()
            .includes(state.todoSearchQuery.toLowerCase()))
      );
    }),
  getTask: state => taskId => state.tasks.find(task => task._id == taskId),
  // getTodosBySection: state => (sectionId) => state.todoArray.filter((task) => task.sectionId == sectionId),
  queriedTags: state =>
    state.taskTags.filter(tag => {
      return tag.name
        .toLowerCase()
        .includes(state.tagSearchQuery.toLowerCase());
    }),

  uncompletedList: state =>
    state.tasks.filter(task => {
      return (
        !task.isCompleted &&
        isBeforeDay(task.startDate || task.createdAt, Date(now()))
      );
    }),

  taskStatistics: state => {
    // Last 7 days
    let d = new Date();
    d.setDate(d.getDate() - 6); // 2021-06-26 => 2021-06-20T18:34:49.817Z

    // ==> Weekly Completed Tasks: (Số lượng task đã check hthành trong tuần)

    const weeklyCompletedTasks = state.tasks.filter(task => {
      let isCompletedInWeek = false;

      if (
        task.isCompleted &&
        isBeforeDay(task.completedDate, new Date()) &&
        isAfterDay(task.completedDate, d)
      ) {
        isCompletedInWeek = true;
      }
      return isCompletedInWeek;
    });

    // ==> Uncompleted Task: (= tasks for today)

    const uncompletedTask = state.tasks.filter(task => {
      return (
        !task.isCompleted &&
        isBeforeDay(task.startDate || task.createdAt, Date(now()))
      );
    });

    // ==> Weekly Tasks: (Số lượng task cả hthành và chưa của tuần này)

    const weeklyTasks = uncompletedTask.concat(weeklyCompletedTasks);

    // ==> Categories: (TT 'categories' cho chart)

    const raw_categories = [];
    const daysOfWeek = convertSelectList(DayEnums);
    daysOfWeek.forEach(day => {
      raw_categories.push(day.shorLabel);
    });

    // ..Fix categories with today:
    // const todayDay = daysOfWeek.find(day => day.id == now().day()).shorLabel;
    const x = raw_categories.slice(); // Ex: x = ["a", "b", "c", "d", "e", "f", "g"]
    const y = x.splice(now().day() + 1); // y = x.slice(3) => x = ["a", "b", "c"], y = ["d", "e", "f", "g"]
    const categories = y.concat(x);

    const sorted_days = []; //mục đích chỉ để duyệt 'series'
    categories.forEach(day => {
      sorted_days.push(DayEnums[day].id);
    });

    // ==> Chart Series: (TT 'chartSeries' cho chart)

    const raw_chartSeries = [];

    const list = weeklyCompletedTasks.slice();
    // group an array of objects (or subarrays) through a key
    const groupsList = Object.values(
      groupBy(list, item => moment(item.completedDate).day())
    );
    const temp = groupsList.map(group => ({
      id: !isNaN(moment(group[0].completedDate).day())
        ? moment(group[0].completedDate).day()
        : -1,
      value: group.length
    }));

    sorted_days.forEach(id => {
      let exist = false;
      temp.forEach(group => {
        if (group.id == id) {
          raw_chartSeries.push(group.value);
          exist = true;
        }
      });
      if (!exist) raw_chartSeries.push(0);
    });

    return {
      completedTasks: weeklyCompletedTasks.length,
      weeklyTotal: weeklyTasks.length,
      chartSeries: raw_chartSeries,
      categories: categories
    };
  },

  queriedStatisticData: state => state.statisticData
};
