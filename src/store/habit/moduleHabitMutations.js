/*=========================================================================================
  File Name: moduleHabitMutations.js
  Description: Habit Module Mutations
==========================================================================================*/

export default {
  SET_HABIT_SEARCH_QUERY(state, query) {
    state.habitSearchQuery = query;
  },

  UPDATE_HABIT_FILTER(state, filter) {
    state.habitFilter = filter;
  },

  // API
  SET_HABITS(state, habits) {
    state.habits = habits;
  },

  ADD_HABIT(state, habit) {
    state.habits.unshift(habit);
  },

  UPDATE_HABIT(state, habit) {
    const habitIndex = state.habits.findIndex(t => t._id == habit._id);
    Object.assign(state.habits[habitIndex], habit);
  },

  SET_DIARY_SEARCH_QUERY(state, query) {
    state.diarySearchQuery = query;
  },

  UPDATE_DIARY_FILTER(state, filter) {
    state.diaryFilter = filter;
  },

  SET_DIARIES(state, habitDiaries) {
    state.habitDiaries = habitDiaries;
  }
};
