/*=========================================================================================
  File Name: moduleHabitGetters.js
  Description: Habit Module Getters
==========================================================================================*/
import moment from "moment";
import _clone from "lodash/cloneDeep";

export default {
  queriedHabits: state =>
    state.habits.filter(habit => {
      return (
        habit._id &&
        habit &&
        habit.name.toLowerCase().includes(state.habitSearchQuery.toLowerCase())
      );
    }),

  queriedDiaries: state =>
    state.habitDiaries.filter(diary => {
      let isItemOfCurrentFilter = false;

      if (
        state.diaryFilter === "all" ||
        moment(diary.time).format("YYYY-MM-DD") == state.diaryFilter ||
        diary.habit._id == state.diaryFilter
        /* state.habits.filter(habit => habit._id == diary.habit._id).length > 0 */
      ) {
        isItemOfCurrentFilter = true;
      }
      return (
        isItemOfCurrentFilter &&
        (diary.text
          .toLowerCase()
          .includes(state.diarySearchQuery.toLowerCase()) ||
          diary.habit.name
            .toLowerCase()
            .includes(state.diarySearchQuery.toLowerCase()))
      );
    }),

  getHabit: state => habitId =>
    state.habits.find(habit => habit._id == habitId),

  getDiary: state => diaryId =>
    state.habitDiaries.find(diary => diary._id == diaryId),

  getHabitDiary: state => diaryId =>
    state.habitDiaries.find(diary => diary._id == diaryId)
};
