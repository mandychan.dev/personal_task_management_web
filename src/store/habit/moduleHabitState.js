/*=========================================================================================
  File Name: moduleHabitState.js
  Description: Habit Module State
==========================================================================================*/

export default {
  habits: [],
  habitSearchQuery: "",
  habitFilter: null,
  habitDiaries: [],
  diarySearchQuery: "",
  diaryFilter: null
};
