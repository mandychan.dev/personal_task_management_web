import Vue from "vue";
import VModal from "vue-js-modal";
import _clone from "lodash/cloneDeep";

import "./styles.scss";
import ModalWrapper from "./ModalWrapper.vue";

import AlertModal from "./AlertModal.vue";
import ConfirmModal from "./ConfirmModal.vue";

export default {
  install(Vue) {
    Vue.use(VModal, {
      dynamic: true,
      injectModalsContainer: true,
      dynamicDefaults: {
        clickToClose: false
      }
    });

    Vue.prototype.$showModal = function(component, params, config = {}) {
      return new Promise(resolve => {
        this.$modal.show(
          ModalWrapper,
          {
            component,
            params: _clone(params),
            onClose: resolve,
            canCloseByBtn:
              config.canCloseByBtn != null ? config.canCloseByBtn : false
          },
          {
            height: "auto",
            scrollable: true,
            clickToClose: !(config.canCloseByBtn != null
              ? config.canCloseByBtn
              : false),
            ...config,
            width: Math.min(
              config.width || 450,
              this.$store.state.windowWidth - 32
            ) // 32 is margin
          },
          {
            closed(event) {
              resolve(null);
            }
          }
        );
      });
    };

    Vue.prototype.$alert = function({ text, btnClose = "Hủy bỏ" }) {
      return this.$showModal(AlertModal, { text, btnClose }, { width: 400 });
    };
    Vue.prototype.$confirm = function({
      text,
      btnYes = "OK",
      btnNo = "Hủy bỏ",
      placeholderInput,
      inputType
    }) {
      return this.$showModal(
        ConfirmModal,
        { text, btnYes, btnNo, placeholderInput, inputType },
        { width: 400 }
      );
    };
  }
};
