export default [
  {
    id: "high-priority",
    name: "Ưu tiên cao",
    color: "red",
    icon: "!",
    textIcon: true
  },
  {
    id: "medium-priority",
    name: "Ưu tiên trung bình",
    color: "yellow",
    icon: "!!",
    textIcon: true
  },
  {
    id: "low-priority",
    name: "Ưu tiên thấp",
    color: "blue",
    icon: "!!!",
    textIcon: true
  },
  {
    id: "no-priority",
    name: "Ưu tiên chưa đặt",
    color: "charcoal",
    icon: "!!!",
    textIcon: true
  },
  /// { id: "f_002", name: "Assigned to others", color: "charcoal" },
  /// { id: "f_003", name: "No due date", color: "grape" },
  { id: "all", name: "Tất cả", color: "charcoal", icon: "LayersIcon" },
  { id: "starred", name: "Dấu sao", color: "charcoal", icon: "StarIcon" },
  { id: "important", name: "Quan trọng", color: "charcoal", icon: "InfoIcon" },
  { id: "completed", name: "Hoàn thành", color: "charcoal", icon: "CheckIcon" },
  { id: "trashed", name: "Thùng rác", color: "charcoal", icon: "TrashIcon" }
];
