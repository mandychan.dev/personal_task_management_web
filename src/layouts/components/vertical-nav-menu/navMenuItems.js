/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  Strucutre:
          url     => router path
          name    => name to display in sidebar
          slug    => router path name
          icon    => Feather Icon component/icon name
          tag     => text to display on badge
          tagColor  => class to apply on badge element
          i18n    => Internationalization
          submenu   => submenu of current item (current item will become dropdown )
                NOTE: Submenu don't have any icon(you can add icon if u want to display)
          isDisabled  => disable sidebar item/group
==========================================================================================*/

export default [
  {
    url: "/dashboard",
    name: "Dashboard",
    slug: "dashboard-general",
    // tag: "2",
    tagColor: "warning",
    icon: "HomeIcon"
  },
  {
    url: "/todo/today",
    name: "Hôm nay",
    slug: "todo-today",
    icon: "event",
    featherIcon: false
  },
  {
    url: "/todo/next-week",
    name: "7 ngày tiếp theo",
    slug: "todo-next-weeks",
    icon: "date_range",
    featherIcon: false
  }
];
