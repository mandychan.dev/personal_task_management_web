// import { serialize } from "object-to-formdata";
//
// const options = {
//   indices: true, // include array indices in FormData keys
//   nullsAsUndefineds: false, // treat null values like undefined values and ignore them
//   booleansAsIntegers: false, // convert true or false to 1 or 0 respectively (defaults to false)
//   allowEmptyArrays: false // store arrays even if they're empty (defaults to false)
// };
//
// export function buildFormData(object, currentFormData, prefix) {
//   // let formData = new FormData();
//   // for( var i = 0; i < this.files.length; i++ ){
//   //   let file = this.files[i];
//   //
//   //   formData.append('files[' + i + ']', file);
//   // }
//   return serialize(
//     object,
//     options, // optional
//     currentFormData, // optional
//     prefix // optional
//   );
// }

import { objectToFormData } from "object-to-formdata";

const options = {
  indices: false,
  nulls: false,
  allowEmptyArrays: true
};

export function buildFormData(object, currentFormData, prefix) {
  const formData = new FormData();
  for (const name in object) {
    if (typeof object[name] === "object") {
      if (name === "files") {
        // Files
        for (let i = 0; i < object.files.length; i++) {
          formData.append(name, object.files[i]);
        }
      } else {
        // Array
        formData.append(name, JSON.stringify(object[name]));
      }
    } else {
      // Strings or Numbers
      formData.append(name, object[name]);
    }
  }
  for (var pair of formData.entries()) {
    console.log(pair[0] + ", " + pair[1]);
  }
  return formData;
}
