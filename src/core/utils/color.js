import colorItems from "../../constants/colorItems.js";
import { PriorityEnums } from "../../constants/enums.js";
import { convertSelectList } from "./list";

export function getRandomColor() {
  const letters = "0123456789ABCDEF";
  let color = "#";

  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }

  return color;
}

export function todoLabelColor(code) {
  return colorItems.find(i => i.value == code);
}

export function todoPriorityColor(_id) {
  return convertSelectList(PriorityEnums).find(i => i.id == _id);
}
