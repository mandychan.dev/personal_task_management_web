import moment from "moment";
import _clone from "lodash/clone";
import { convertSelectList } from "../../core/utils/list";
import { DayEnums } from "../../constants/enums";

moment.locale("vi");

export function formatTextDate(value, format = "DD/MM/YYYY") {
  return moment(value).format(format);
}

export function formatTextDateTime(value, format = "DD/MM/YYYY HH:mm") {
  return moment(value).format(format);
}

export function formatTextTimeDate(value, format = "HH:mm DD/MM/YYYY") {
  return moment(value).format(format);
}

export function formatTextTime(value, format = "HH:mm") {
  return moment(value).format(format);
}

export function formatTextYear(value, format = "YYYY") {
  return moment(value).format(format);
}

export function formatTextTimeAgo(value) {
  return moment(value).fromNow();
}

export function displayDuration(from, to) {
  const result = moment.utc(Math.max(moment(to).diff(moment(from)), 60000));

  if (result.hours() === 0) {
    return result.format("m[ mins]");
  }

  return result.format("H[ hour(s) ]m[ mins]");
}

export function sortedDates(list) {
  return list.sort((a, b) =>
    moment(new Date(a).toUTCString()).isBefore(new Date(b).toUTCString())
      ? -1
      : 1
  );
}

export function isBeforeDay(a, b) {
  return moment(a).isBefore(b);
}

export function isAfterDay(a, b) {
  return moment(a).isAfter(b);
}

export function isSameDay(a, b) {
  return (
    moment(a).isSame(b, "day") &&
    moment(a).isSame(b, "month") &&
    moment(a).isSame(b, "year")
  );
}

export function isSameTime(a, b) {
  return moment(a).isSame(b);
}

export function isBigDaysRange(from, to) {
  const daysDifferent = moment(to).diff(from, "days");
  return daysDifferent >= 1;
}

export function now() {
  return moment();
}

export function getDayOfToday() {
  const daysOfWeek = convertSelectList(DayEnums);
  return daysOfWeek.find(item => item.id == moment().day());
}

export function getFriendlyDate(value) {
  return `${moment(value).date()} Th${moment(value).month() + 1}`;
}

export function getLogestStreak(list) {
  if (list.length <= 1) return list.length;

  const streaks = [];
  let count = 0;

  const sorted = list.sort((a, b) => (moment(a).isBefore(b) ? -1 : 1));

  for (let i = 0; i < sorted.length - 1; i++) {
    const a = sorted[i];
    const b = sorted[i + 1];
    if (
      moment(a).isSame(b, "month") &&
      moment(a).isSame(b, "year") &&
      moment(b).date() - moment(a).date() == 1
    ) {
      count++;
    } else {
      streaks.push(count);
      count = 0;
    }
  }

  if (count != 0) {
    streaks.push(count);
  }

  return Math.max(...streaks) + 1;
}

export function getCurrentStreak(list) {
  if (list.length <= 1) return list.length;

  let count = 0;

  const sorted = list.sort((a, b) => (moment(a).isBefore(b) ? -1 : 1));

  for (let i = sorted.length - 1; i >= 1; i--) {
    const a = sorted[i];
    const b = sorted[i - 1];
    if (
      moment(a).isSame(b, "month") &&
      moment(a).isSame(b, "year") &&
      moment(a).date() - moment(b).date() == 1
    ) {
      count++;
    } else {
      break;
    }
  }

  return count + 1;
}

// Method for creating dummy notification time
export function randomDate({ hr, min, sec }) {
  let date = new Date();

  if (hr) date.setHours(date.getHours() - hr);
  if (min) date.setMinutes(date.getMinutes() - min);
  if (sec) date.setSeconds(date.getSeconds() - sec);

  return date;
}
