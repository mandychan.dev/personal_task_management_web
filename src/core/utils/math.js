export function toPercentText(x, total) {
  return ((x / total) * 100).toFixed(2);
}

export function toPercent(x, total) {
  return (x / total) * 100;
}

export function round10(value, exp = -1) {
  const decimalAdjust = (type, value, exp) => {
    // If the exp is undefined or zero...
    if (typeof exp === "undefined" || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // If the value is not a number or the exp is not an integer...
    if (isNaN(value) || !(typeof exp === "number" && exp % 1 === 0)) {
      return NaN;
    }
    // Shift
    value = value.toString().split("e");
    value = Math[type](+(value[0] + "e" + (value[1] ? +value[1] - exp : -exp)));
    // Shift back
    value = value.toString().split("e");
    return +(value[0] + "e" + (value[1] ? +value[1] + exp : exp));
  };

  // Decimal round
  return decimalAdjust("round", value, exp);
}

/// // Round
/// Math.round10(55.55, -1);   // 55.6
/// Math.round10(55.549, -1);  // 55.5
/// Math.round10(55, 1);       // 60
/// Math.round10(54.9, 1);     // 50
/// Math.round10(-55.55, -1);  // -55.5
/// Math.round10(-55.551, -1); // -55.6
/// Math.round10(-55, 1);      // -50
/// Math.round10(-55.1, 1);    // -60
