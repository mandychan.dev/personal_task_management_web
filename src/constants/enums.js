export const HabitTypeEnum = {
  Type1: { label: "Thể loại 1", id: 0 },
  Type2: { label: "Thể loại 2", id: 1 },
  Type3: { label: "Thể loại 3", id: 2 },
  Type4: { label: "Thể loại 4", id: 3 }
};

export const HabitGoalEnums = {
  ArchiveItAll: { id: 0, label: "Đạt 1 lần cho tất cả" },
  ReachACertainAmount: { id: 1, label: "Đạt đến số lượng nhất định" }
};

export const HabitFrequencyEnums = {
  daily: { id: "daily", label: "Hằng ngày", value: "daily" },
  weekly: { id: "weekly", label: "Hằng tuần", value: "weekly" }
  /* interval: { id: 2, label: "Khoảng thời gian" } */
};

export const HabitDailyDaysEnums = {
  T2: 0,
  T3: 1,
  T4: 2,
  T5: 3,
  T6: 4,
  T7: 5,
  CN: 6
};

export const HabitDayOfWeekEnums = {
  0: "CN",
  1: "T2",
  2: "T3",
  3: "T4",
  4: "T5",
  5: "T6",
  6: "T7"
};

export const HabitMissionDayUnitEnum = {
  Count: { id: 0, label: "Lần" },
  Cup: { id: 1, label: "Cốc" },
  Milliliter: { id: 2, label: "Ml" },
  Minute: { id: 3, label: "Phút" },
  Hour: { id: 4, label: "Giờ" },
  Kilometer: { id: 5, label: "Km" },
  Page: { id: 6, label: "Trang" }
};

export const HabitMissionDayCheckInEnum = {
  Auto: { id: 0, label: "Tự động" },
  Manual: { id: 1, label: "Bằng tay" }
  // CompleteAll: { id: 2, label: "Hoàn thành tất cả" }
};

export const PriorityEnums = {
  HighPriority: {
    id: 0,
    label: "Ưu tiên cao",
    textIcon: true,
    icon: "!",
    color: "red"
  },
  MediumPriority: {
    id: 1,
    label: "Ưu tiên trung bình",
    textIcon: true,
    icon: "!!",
    color: "yellow"
  },
  LowPriority: {
    id: 2,
    label: "Ưu tiên thấp",
    textIcon: true,
    icon: "!!!",
    color: "blue"
  },
  NoPriority: {
    id: 3,
    label: "Không đặt",
    textIcon: true,
    icon: "!!!",
    color: "charcoal"
  }
};

export const MonthEnums = {
  Jan: "Tháng 1",
  Feb: "Tháng 2",
  Mar: "Tháng 3",
  Apr: "Tháng 4",
  May: "Tháng 5",
  Jun: "Tháng 6",
  Jul: "Tháng 7",
  Aug: "Tháng 8",
  Sep: "Tháng 9",
  Oct: "Tháng 10",
  Nov: "Tháng 11",
  Dec: "Tháng 12"
};

export const DayEnums = {
  CN: { id: 0, label: "Chủ Nhật", shorLabel: "CN" },
  T2: { id: 1, label: "Thứ 2", shorLabel: "T2" },
  T3: { id: 2, label: "Thứ 3", shorLabel: "T3" },
  T4: { id: 3, label: "Thứ 4", shorLabel: "T4" },
  T5: { id: 4, label: "Thứ 5", shorLabel: "T5" },
  T6: { id: 5, label: "Thứ 6", shorLabel: "T6" },
  T7: { id: 6, label: "Thứ 7", shorLabel: "T7" }
};

export const ViewTypeEnums = {
  List: 0
  // Board: 1
};
