export default [
  {
    label: "Berry Red",
    color: "rgb(184, 37, 95)",
    value: "berry-red"
  },
  {
    label: "Red",
    color: "rgb(219, 64, 53)",
    value: "red"
  },
  {
    label: "Orange",
    color: "rgb(255, 153, 51)",
    value: "orange"
  },
  {
    label: "Yellow",
    color: "rgb(250, 208, 0)",
    value: "yellow"
  },
  {
    label: "Olive Green",
    color: "rgb(175, 184, 59)",
    value: "olive-green"
  },
  {
    label: "Lime Green",
    color: "rgb(126, 204, 73)",
    value: "lime-green"
  },
  {
    label: "Green",
    color: "rgb(41, 148, 56)",
    value: "green"
  },
  {
    label: "Mint Green",
    color: "rgb(106, 204, 188)",
    value: "mint-green"
  },
  {
    label: "Teal",
    color: "rgb(21, 143, 173)",
    value: "teal"
  },
  {
    label: "Sky Blue",
    color: "rgb(20, 170, 245)",
    value: "sky-blue"
  },
  {
    label: "Light Blue",
    color: "rgb(150, 195, 235)",
    value: "light-blue"
  },
  {
    label: "Blue",
    color: "rgb(64, 115, 255)",
    value: "blue"
  },
  {
    label: "Grape",
    color: "rgb(136, 77, 255)",
    value: "grape"
  },
  {
    label: "Violet",
    color: "rgb(175, 56, 235)",
    value: "violet"
  },
  {
    label: "Lavender",
    color: "rgb(235, 150, 235)",
    value: "lavender"
  },
  {
    label: "Magenta",
    color: "rgb(224, 81, 148)",
    value: "magenta"
  },
  {
    label: "Salmon",
    color: "rgb(255, 141, 133)",
    value: "salmon"
  },
  {
    label: "Charcoal",
    color: "rgb(128, 128, 128)",
    value: "charcoal"
  },
  {
    label: "Grey",
    color: "rgb(184, 184, 184)",
    value: "grey"
  },
  {
    label: "Taupe",
    color: "rgb(204, 172, 147)",
    value: "taupe"
  },
  {
    label: "Dark",
    color: "rgb(44, 43, 53)",
    value: "dark"
  }
];
