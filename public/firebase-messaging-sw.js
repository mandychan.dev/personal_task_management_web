// Scripts for firebase and firebase messaging
importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js");

// Initialize the Firebase app in the service worker by passing the generated config
const firebaseConfig = {
  apiKey: "AIzaSyCkXHF9KI76x-H2T8KWgBB9ko3_Y8NcQuo",
  authDomain: "personal-task-management-db7dc.firebaseapp.com",
  databaseURL:
    "https://personal-task-management-db7dc-default-rtdb.firebaseio.com",
  projectId: "personal-task-management-db7dc",
  storageBucket: "personal-task-management-db7dc.appspot.com",
  messagingSenderId: "869499037151",
  appId: "1:869499037151:web:d82f822c3c1fbbe920906a",
  measurementId: "G-XP4G0CVWY7"
};
firebase.initializeApp(firebaseConfig);

// Retrieve firebase messaging
const messaging = firebase.messaging();

messaging.onBackgroundMessage(function(payload) {
  console.log("Received background message ", payload);

  const notificationTitle = payload.notification.title;
  const notificationOptions = {
    body: payload.notification.body
  };

  self.registration.showNotification(notificationTitle, notificationOptions);
});
