# Personal Task Management Web
The app for our users.

Totodo - Introduction - YouTube: https://www.youtube.com/watch?v=0jHPY6UfMrE

Live site: https://personal-task-management-web.herokuapp.com/

![Personal Task Management Application](./public/app.PNG)

## Getting Started

This project is a VueJS web application.

A few resources to get you started if this is your first VueJS project:
- [Vue CLI](https://cli.vuejs.org/)
- [Vuex](https://vuex.vuejs.org/guide/)
- [Vuesax](https://lusaxweb.github.io/vuesax/components/)

## Setup

1. Create a file name .env with following content:

```
PORT=3032

VUE_APP_ENV_LOGIN_EMAIL=totodo.test@gmail.com
VUE_APP_ENV_LOGIN_PASSWORD=Abc123

VUE_APP_ENV_API_URL=https://personal-task-management-be.herokuapp.com

VUE_APP_ENV_STATIC_HOST=
VUE_APP_ENV_WEBVIEW_HOST=

SHOW_DEBUG_LOG=true
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint -- --fix
```

### Deployment
```
$ npm run build

$ heroku login

$ heroku git:clone -a personal-task-management-web
$ heroku git:remote -a personal-task-management-web
$ git add .
$ git commit -am "<commit_name>"

$ heroku buildpacks:add heroku/nodejs (optional)
$ git push heroku <branch_name>:master
```
