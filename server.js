const express = require("express");
const serveStatic = require("serve-static");
const path = require("path");

const app = express();

// configure dist to serve app files
app.use("/", serveStatic(path.join(__dirname, "/dist")));

// this * route is to serve project on different page routes except root `/`
app.get(/.*/, function(req, res) {
  res.sendFile(path.join(__dirname, "/dist/index.html"));
});

const port = process.env.PORT || 3032;
app.listen(port);
console.log(`Server is listening on port: ${port}`);
